At William Linger, DDS, MAGD we designed our office with our patients in mind. By combining a calm atmosphere with modern technology, we have created a place where our patients in Charlotte receive the quality of dental care they need and deserve.

Address: 435 N Wendover Road, Charlotte, NC 28211, USA

Phone: 704-364-2510